import getopt, sys, time

kullanici_girdisi = ""
zor_sifre = "GIZLI"
karsilastirma_sifre = ""


def bilgi_al(argv):
    global kullanici_girdisi
    try:
        opts, args = getopt.getopt(argv, "hs:", ["sifre="])
    except getopt.GetoptError:
        print("%s -s <sifre>" % sys.argv[0])
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print("%s -s <sifre>" % sys.argv[0])
            sys.exit()
        elif opt in ("-s", "--sifre"):
            kullanici_girdisi = arg


def cok_guclu_sifre_cozucu(ind, char):
    if zor_sifre[ind] == char:
        # print("Cozuluyor...")
        time.sleep(1)
        return char
    else:
        return ""


if __name__ == "__main__":
    baslangic_zaman = time.time()

    bilgi_al(sys.argv[1:])
    print("Kullanıcı Girdisi : %s" % kullanici_girdisi)

    for idx, ch in enumerate(kullanici_girdisi):
        karsilastirma_sifre += cok_guclu_sifre_cozucu(idx, ch)

    if karsilastirma_sifre == zor_sifre:
        print("Tebrikler sifre dogru.")
    bitis_zaman = time.time()

    print("Toplam süre: %s" % (bitis_zaman - baslangic_zaman))
